// Initializes the `lusers` service on path `/lusers`
const { Lusers } = require('./lusers.class');
const createModel = require('../../models/lusers.model');
const hooks = require('./lusers.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/lusers', new Lusers(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('lusers');

  service.hooks(hooks);
};
